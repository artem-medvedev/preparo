var gulp = require('gulp'),
    path = require('path'),
    less = require('gulp-less'),
    fileinclude = require('gulp-file-include'),
    concat = require('gulp-concat'),
    csso = require('gulp-csso'),
    cmq = require('gulp-combine-media-queries'),
    autoprefixer = require('gulp-autoprefixer'),
    pngquant = require('imagemin-pngquant'),
    optipng = require('imagemin-optipng'),
    imagemin = require('gulp-imagemin'),
    svgo = require('imagemin-svgo'),
    rename = require("gulp-rename"),
    browserSync = require('browser-sync').create();

var bc = './bower_components/';

// HTML Task
gulp.task('html', function() {
    // Landing main page
    gulp.src(['dev/index-landing.html'])
        .pipe(fileinclude({
            prefix: '@@',
        }))
        .pipe(rename("index.html"))
        .pipe(gulp.dest('./build'))
        .pipe(browserSync.stream());

    // Recruiter landing main page
    gulp.src(['dev/index-recruiter.html'])
        .pipe(fileinclude({
            prefix: '@@',
        }))
        .pipe(rename("recruiter.html"))
        .pipe(gulp.dest('./build'))
        .pipe(browserSync.stream());

    // Additional pages
    gulp.src(['dev/html/pages/*.html'])
        .pipe(fileinclude({
            prefix: '@@',
        }))
        .pipe(gulp.dest('./build/pages'))
        .pipe(browserSync.stream());
});


// Less task
gulp.task('less', function() {
    gulp.src('./dev/less/style.less')
        .pipe(less({
            paths: ['./**/*.less']
        }))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(cmq()) // Combine media queries
        .pipe(concat('style.min.css'))
        // .pipe(csso()) // CSS minimizer
        .pipe(gulp.dest('build/css/'))
        .pipe(browserSync.stream());
});


// Scripts Task
gulp.task('scripts', function() {
    gulp.src([
            'dev/scripts/landing.js',
            'dev/scripts/sidebar.js',
            'dev/scripts/policies.js',
            'dev/scripts/forms.js'
        ])
        .pipe(concat('main.js'))
        .pipe(gulp.dest('build/scripts'))
        .pipe(browserSync.stream());

    gulp.src('dev/scripts/analytics.js')
        .pipe(gulp.dest('build/scripts'));
});


// Libs Task
gulp.task('libs', function() {
    gulp.src(bc+'jquery/dist/jquery.min.js')
        .pipe(gulp.dest('build/libs/jquery/'));

    gulp.src(bc + 'bootstrap/dist/**/*.*')
        .pipe(gulp.dest('build/libs/bootstrap/'));

    gulp.src(bc + 'font-awesome/css/**/*.*')
        .pipe(gulp.dest('build/libs/font-awesome/css'));
    gulp.src(bc + 'font-awesome/fonts/**/*.*')
        .pipe(gulp.dest('build/libs/font-awesome/fonts'));

    gulp.src(bc + 'tap/dist/tap.min.js')
        .pipe(gulp.dest('build/libs/tap/'));
});


// Compress images
gulp.task('images', function() {
    gulp.src('dev/images/**/*.{png,jpg,jpeg,gif,svg}')
        .pipe(svgo()())
        .pipe(optipng({ optimizationLevel: 3 })())
        .pipe(pngquant({ quality: '65-80', speed: 4 })())
        .pipe(imagemin({ progressive: true }))
        .pipe(gulp.dest('build/images/'));
});


// Watch Task
gulp.task('watch', function() {
    gulp.watch('dev/**/*.html', ['html']);
    gulp.watch('dev/**/*.js', ['scripts']);
    gulp.watch('dev/less/**/*.less', ['less']);
    gulp.watch('dev/images/**/*.*', ['images']);
});


// Browsersync Task
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./build/"
        },
        browser: 'Google Chrome' // OS X : 'Google Chrome', Windows : 'chrome'
    });
});


// Default task
gulp.task('default', [
    'html',
    'less',
    'scripts',
    'libs',
    'images',
    'browser-sync',
    'watch'
]);