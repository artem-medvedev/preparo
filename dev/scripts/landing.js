$(document).ready(function() {	
	// Show/hide signin popup
	var signup = document.querySelector('#signup');
	if ( signup !== null ) {
		var form = document.querySelector('.dropdown-form'),
			fields = form.getElementsByTagName('input');

		var clickOutside = function(e) {
			if (!$(e.target).closest('.dropdown-form').length) {
				form.classList.remove('open');

				this.removeEventListener('click', clickOutside, false);
			}
		};

		var closeOnEsc = function(e) {
			if (e.keyCode === 27) { 
		        form.classList.remove('open');
		        this.removeEventListener(e.type, closeOnEsc, false);
		    }
		};

		var loginDropdown = function(e) {
			if ( window.matchMedia('(min-width: 992px)').matches ) {
				e.preventDefault();
				e.stopPropagation();

				for ( var i = 0; i < fields.length; i++ ) {
					fields[i].addEventListener('keyup', closeOnEsc, false);
				}

				form.classList.toggle('open');
				fields[0].focus();

				document.addEventListener('click', clickOutside, false);
			}
		};

		signup.addEventListener('click', function(e) {
			loginDropdown(e);	
		});
	}

	if ( window.matchMedia('(max-width: 992px)').matches ) {
		$('.check-box').on('tap', function(e) {
			e.preventDefault();
			// console.log('Clicked');
			$(this).siblings('input').prop("checked", !$(this).siblings('input').prop("checked"));
		});
	}


	

	// Carousels initialization
	$("#carousel").carousel({
		interval: 4000
	});

	$('#clientsCarousel').carousel({
		interval: 3000
	});

	// Multiitem carousel
	$('.carousel-showmanymoveone .item').each(function () {
		var itemToClone = $(this);

		for (var i = 1; i < 4; i++) {
			itemToClone = itemToClone.next();

			// wrap around if at end of item collection
			if (!itemToClone.length) {
				itemToClone = $(this).siblings(':first');
			}

			// grab item, clone, add marker class, add to collection
			itemToClone.children(':first-child').clone()
				.addClass("cloneditem-" + (i))
				.appendTo($(this));
		}
	});


	// Get position for active slide arrow
	var activePos = function() {
		var activeIndicator = document.querySelector('.carousel-indicators > li.active'),
		    activeOffset = (activeIndicator.offsetParent.offsetLeft + activeIndicator.offsetLeft),
		    indicatorWidth = activeIndicator.clientWidth / 2,
		    position = indicatorWidth + activeOffset;
		
		return position;
	};

	// Set position for active slide arrow
	function slideArrow() {
		var arrow = document.querySelector('.carousel-arrow');
		arrow.style.cssText = 'display: block; left: ' + activePos() + 'px';
	}



	// Smooth scroll to anchor
	$('a[href*=#]:not([href=#]):not(.carousel-control)').on('tap', function() {
		$(this).blur();
		if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top
				}, 400);
				return false;
			}
		}
	});


	// Scroll to register
	$(".register").click(function(e) {
	    e.preventDefault();

	    var smallScreen = $("#register").offset().top - $("header").height(),
	    	largeScreen = $(document).height() - $(window).height() - $("footer").height();

	    if ( $(window).width() > 1440 ) {
	    	$('html,body').animate({scrollTop: largeScreen});
	    } else {
	    	$('html,body').animate({scrollTop: smallScreen});
	    }

	});


	// Toggle menu on devices on tap
	function collapseMenu() {
		if (window.matchMedia('(max-width:768px)').matches) {
			$('.menu-link').click(function() {
				$('.navbar-collapse').collapse('hide');
			});

			var toggleNavbar = function() {
				$('.navbar-collapse').collapse('hide');
				document.removeEventListener('tap', toggleNavbar,false);
			};

			$('.navbar-collapse').on('show.bs.collapse', function() {
				document.addEventListener('tap', toggleNavbar,false);
			});
		}
	}


	//Animate on scroll
	function animateFeatures(el) {
		var windowHeight = $(window).height(),
			waypoint = $(el).position().top + (windowHeight - 200),
			scroll = $(window).scrollTop();
		
		if ( scroll > waypoint) {
			$(el).find(".features-about").css("display","block");
			$(el).find(".features-preview").css("display","block");
		}
	}

	function animateRegister() {
		var $el = $("#register"),
	        windowHeight = $(window).height() / 2,
			waypoint = $el.position().top - windowHeight,
			scroll = $(window).scrollTop();
		
		if ( scroll > waypoint) {
			$el.find("img").addClass("flipIn");
			$el.find(".register-text").addClass("fadeInUp");
			$el.find(".register-input").addClass("bounceInLeft");
		}
	}

	function animateFooter() {
	    if ( window.matchMedia("(min-width: 768px)").matches ) {
	    	var footerOffset = $("footer").offset().top,
	            scroll = $(window).scrollTop(),
	            windowHeight = $(window).height()-300,
	            waypoint = footerOffset - scroll;
			
			if ( windowHeight > waypoint) {
				$("footer .footer-wrap").fadeIn();
			} else {
	            $("footer .footer-wrap").fadeOut();
	        }
	    }
	}


	// SVG Fallback for IE
	function detectIE() {
	    if ( navigator.userAgent.match(/MSIE|Trident|Edge/) ) {
	        $(".svg").removeClass("svg").addClass("no-svg");
	    }
	}


	// Subscribe section
	function showInput() {
		$("#submitSubscribe").css("transform","translateX(80%)").blur();
		
		$("#inputTitle").addClass('collapsed').html("Subscribe");

		$("#registerInput").css("transform","translateX(0)");
	}

	function hideInput() {
		$("#submitSubscribe").css("transform","translateX(0)");

		$("#inputTitle").removeClass('collapsed').html("Emailadresse");

		$("#registerInput").css("transform","translateX(-80%)");
	}

	$("#inputTitle").click(function() {
		if ( !$(this).hasClass('collapsed') ) {
			showInput();

			// Hide input when click esc
			$(document).keyup(function(e) {
			     if (e.keyCode === 27) {
			        hideInput();
			    }
			});
		} else {
			$("#subscribe").submit();
		}
	});

	// Hide input when click outside
	$("#register").click(function(e) {
		if ( !$(e.target).is('#inputTitle, #registerInput') ) {
			if ( $("#inputTitle").hasClass('collapsed') ) {
				hideInput();
			}
		}
	});

	// Submit on press Enter
	$("#registerInput").keypress(function(e) {
	    if (e.keyCode === 13) {
	        e.preventDefault();
	        $("#subscribe").submit();
	    }
	});

	// Show final text
	$("#subscribe").submit(function() {
		$(".register-input").fadeOut();
		$(this).find("p").fadeIn();
	});


	// Call functions

	detectIE();
	// Need to merge
	if ( $("#landing-features").length ) {
		animateFeatures("#video");
		animateFeatures("#oneClick");
		animateFeatures("#tracking");

		$(window).scroll(function() {
	        animateRegister();
	        
	        animateFeatures("#video");
	        animateFeatures("#oneClick");
	        animateFeatures("#tracking");
	        
	        animateFooter();
		});

		$(window).resize(function() {
			slideArrow();
		});
	}
	// Need to merge
	if ( $("#recruiter-features").length ) {
		animateFeatures("#video");
		animateFeatures("#oneClick");
		animateFeatures("#tracking");

		$(window).scroll(function() {
	        animateFeatures("#video");
	        animateFeatures("#oneClick");
	        animateFeatures("#tracking");
	        
	        animateFooter();
		});

		$(window).resize(function() {
			slideArrow();
		});
	}

	// Need to merge
	if ( $("#carousel").length ) {
		// Active slide arrow
		slideArrow();

		$("#carousel").on('slide.bs.carousel', function () {
			setTimeout(function(){ 
				slideArrow();
			}, 200);
		});
	}
	// Need to merge
	if ( $("#carouselb2b").length ) {
		// Active slide arrow
		slideArrow();

		$("#carouselb2b").on('slide.bs.carousel', function () {
			setTimeout(function(){ 
				slideArrow();
			}, 200);
		});
	}

	collapseMenu();

	// $(window).resize(function() {
	// 	collapseMenu();
	// });