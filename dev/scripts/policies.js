
    // Run script on policies page
    var policies = document.querySelector('#policies');
    if ( policies !== null ) {
        $(".policy-section").click(function(e) {
            e.preventDefault();
            
            $('.subtitle-list').slideUp();

            if (!$(this).find('.subtitle-list').is(":visible")) {
                $(this).find(".subtitle-list").slideToggle();
            }
        });
    }
