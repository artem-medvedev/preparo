
	// Sidebar
	if ( $("#sidebar").length ) {
	    var lastScrollTop = 0;


	    // var scrollTop = (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop,
		// var position = document.body.scrollTop;

		// function scrollD() {
		//     var scroll = document.body.scrollTop;

		//     if (scroll > position) {
		//         console.log('scroll Down');
		//     } else {
		//         console.log('scroll Up');
		//     }
		//     position = scroll;
		// }


		$(window).scroll(function() {
			var st = $(this).scrollTop(),
				$sidebar = $("#sidebar"),
				ot = $sidebar.offset().top,
				topWaypoint = $(".about").offset().top,
				bottomWaypoint = ot + $sidebar.height(),
				footerOffset = $("#footer").offset().top;

			// Scroll down
			if ( st > lastScrollTop ) {
				if ( ot >= topWaypoint ) {
					$sidebar.removeClass('fadeOutLeft').addClass('fadeInLeft');
				}

				if ( bottomWaypoint >= footerOffset ) {
					$sidebar.removeClass('fadeInLeft').addClass('fadeOutLeft');
				}
			}

			// Scroll up
			else {
				if ( ot <= topWaypoint ) {
					$sidebar.removeClass('fadeInLeft').addClass('fadeOutLeft');
				}

				if ( bottomWaypoint >= footerOffset ) {
					$sidebar.removeClass('fadeOutLeft').addClass('fadeInLeft');
				}
			}

			lastScrollTop = st;
		});

		// Open/hide sidebar on mobile
		var $collapse = $("#collapse");

		$collapse.click(function(e) {
			e.preventDefault();
			// console.log("msg");

			$("#sidebar ul").toggleClass('open');
			// $("#sidebar ul").slideToggle(300);
		});
	}
	