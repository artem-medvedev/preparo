if ( $("#sidebar").length ) {
	var $sidebar = $("#sidebar");

	function toggleSidebar() {
		var sidebarOffsetTop = $sidebar.offset().top,
			footerOffset = $("#footer").offset().top,
			topWaypoint = $("#features").offset().top,
			bottomWaypoint = footerOffset - $sidebar.height();

		if ( sidebarOffsetTop > topWaypoint && sidebarOffsetTop < bottomWaypoint) {
			$sidebar.removeClass('fadeOutLeft').addClass('fadeInLeft');
		} 
		else {
			$sidebar.removeClass('fadeInLeft').addClass('fadeOutLeft');
		}
	}

	$(window).scroll(function() {
		var sidebarOffsetTop = $sidebar.offset().top,
			topWaypoint = $("#features").offset().top;

		if ( sidebarOffsetTop >= topWaypoint ) {
			toggleSidebar();
		}
	});
}