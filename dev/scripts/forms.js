	
	// Forms
	function alignForm() {
		var formAlign = document.querySelector('.form-align'),
			form = formAlign.getElementsByTagName('form')[0],
			formHeight = form.clientHeight;

		formAlign.style.cssText = "margin-top:" + -(formHeight/2) + "px; visibility: visible;";
		form.classList.add('fadeIn');
	}

	var formAlign = document.querySelector('.form-align');

	if ( formAlign !== null ) {
		alignForm();

		$(window).resize(function() {
			alignForm();
		});
	}

});