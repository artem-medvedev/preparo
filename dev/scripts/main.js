var body = document.body,
    timer;

window.addEventListener('scroll', function() {
    clearTimeout(timer);
    if (!body.classList.contains('disable-hover')) {
        body.classList.add('disable-hover');
    }

    timer = setTimeout(function() {
        body.classList.remove('disable-hover');
    }, 500);
}, false);

// Remove :hover events on devices
// var touch = window.ontouchstart
//         || navigator.MaxTouchPoints > 0
//         || navigator.msMaxTouchPoints > 0;

// if (touch) { // remove all :hover stylesheets
//     try { // prevent crash on browsers not supporting DOM styleSheets properly
//         for (var si in document.styleSheets) {
//             var styleSheet = document.styleSheets[si];
//             if (!styleSheet.rules) continue;

//             for (var ri = styleSheet.rules.length - 1; ri >= 0; ri--) {
//                 if (!styleSheet.rules[ri].selectorText) continue;

//                 if (styleSheet.rules[ri].selectorText.match(':hover')) {
//                     stylesheet.deleteRule(ri);
//                 }
//             }
//         }
//     } catch (ex) {}
// }